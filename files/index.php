<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Мой сайт</title>
</head>
<body>
    <h1>Мой сайт</h1>

    <?php
        $servername = "localhost";
        $username = "admin"; // замените на ваше имя пользователя БД
        $password = "secret"; // замените на ваш пароль БД
        $dbname = "mydb";

        // Создаем подключение
        $conn = new mysqli($servername, $username, $password, $dbname);

        // Проверяем подключение
        if ($conn->connect_error) {
            die("Подключение не удалось: " . $conn->connect_error);
        }

        // Выполняем запрос к базе данных
        $sql = "SELECT * FROM first";
        $result = $conn->query($sql);




        if ($result->num_rows > 0) {
            // Вывод данных каждой строки
            while($row = $result->fetch_assoc()) {
                echo "<p>Сообщение: " . $row["name"]. "</p>";
            }
        } else {
            echo "0 результатов";
        }

        $conn->close();
    ?>
</body>
</html>